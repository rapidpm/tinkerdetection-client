package org.rapidpm.tinkerdetection;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Date;

/**
 * TinkerDetection - Client
 * A project by RapidPM
 * http://www.rapidpm.org
 *
 * User: Marco Ebbinghaus
 * Date: 03.12.2014
 * Time: 17:04
 */
public class Main {
    public static void main(String[] args) {
        try{
            final Context jndiContext = new InitialContext();
            final ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("jms/tinkerdetection/ConnectionFactory");
            final Destination queue = (Destination) jndiContext.lookup("jms/tinkerdetection/queue");
            try(final JMSContext context = connectionFactory.createContext()){
                context.createProducer().send(queue, "Text message sent at " + new Date());
            }
        } catch (final NamingException e){
            e.printStackTrace();
        }

    }
}
